#include <stdio.h>
#include <string.h>
int readInput(char *fname, char *mode);
void syntaxError(char *msg);
int inputEmpty();
int ausgabe(char *msg, int t);
int num(int t);
int expression(int t);
int rightExpression(int t);
int term(int t);
int rightTerm(int t);
int operator(int t);
int digit(int t);
int match(char *matchSet, int t);
int lookAhead(char *aheadSet);

FILE *ptr;
char ch;
char input[256];
char *filename = "testdatei.txt";
char *readmode = "r";
int maxPointer = 0;
int pointer = 0;
int debug = 0;
char digitSet[] = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};


int main()
{
  
  if (readInput(filename, readmode))
  {
    if (!debug)
    {
      if (expression(0) && inputEmpty())
      {
        printf("Korrekter Ausdruck\n");
      }
      else
      {
        printf("Fehler im Ausdruck\n");
      }
      return 0;
    }
  }
  return 0;
}

int readInput(char *fname, char *mode)
{
  int c = 0;
  ptr = fopen(filename, mode);
  if (ptr == NULL)
  {
    return 0;
  }

  else
  {

    for (int i = 0; i < 256; i++)
    {
      c = fgetc(ptr);
      if (c == -1)
      {
        if (debug)
        {
          //printf("DEBUG:INPUT %d 1\n", i);
        }
        maxPointer = i;
        input[i] = EOF;
        break;
      }
      else
      {
        if (debug)
        {
          //printf("DEBUG:INPUT %d 2\n", i);
        }

        input[i] = (char)c;
       
      }
    }
    return 1;
  }
}

int inputEmpty()
{
  if (pointer == maxPointer)
  {
    // ausgabe funktion mit eingabe leer
    ausgabe("eingabe leer",0);
    return 1;
  }
  else
  {
    // syntaxfehler funktionm mit eingfabe bei ende nicht leer
    return 0;
  }
}

void syntaxError(char *msg)
{
  char *message = msg;
  printf("Syntax Error near %d char\n", pointer);
  printf("%s\n", message);
} // syntaxError

int ausgabe(char *msg, int t)
{
  for (int i = 0; i < t; i++)
  {
    printf(" ");
  }
  printf("%s\n", msg);
  return 0;
}

int lookAhead(char * aheadSet)
{
  
  for (int i = 0; i < sizeof(aheadSet); i++)
  {
    if (input[pointer + 1] == aheadSet[i])
    {
      return 1;
    }
  }
  return 0;
}

int match(char *matchSet, int t)
{
  //printf("DEBUG:input:%c\n ", input[pointer]);
  for (int i = 0; i < sizeof(matchSet); i++)
  {
   // printf("DEBUG:matchset%c \n", matchSet[i]);
    if (input[pointer] == matchSet[i])
    {
      char msg[20] = "match";
      strncat(msg, &input[pointer], 1);
      ausgabe(msg, t);
      pointer ++;
      return 1;
    }
  }return 0;
  
}

int digit(int t)
{
  char matchSet[] = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
  ausgabe("digit->", t);
  if (match(matchSet, t + 1))
  {
    return 1;
  }
  else
  {
    syntaxError("Ziffer erwartet");
    return 0;
  }
}

int operator(int t)
{
  char klammerSet[] = {'(', ')'};
  ausgabe("operator->", t);
  if (match(klammerSet, t + 1))
  {
    int return1 = expression(t + 1);
    int return2 = match(klammerSet, t + 1);
    if (!return2)
    {
      syntaxError("geschlossene Klammer erwartet");
    }
    return return1 && return2;
  }
  else
  {
    return num(t + 1);
  }
}

int term(int t)
{
  ausgabe("term->", t);
  return operator(t + 1) && rightTerm(t + 1);
}

int rightTerm(int t)
{
  char punktoperatoren[] = {'*', '/'};
  ausgabe("rightTerm->", t);
  if (match(punktoperatoren, t + 1))
  {
    return operator(t + 1) && rightTerm(t + 1);
  }
  else
  {
    ausgabe("$", t + 1);
    return 1;
  }
}

int rightExpression(int t)
{
  char strichrechnung[] = {'+', '-'};
  ausgabe("rightExpression->", t);
  if (match(strichrechnung, t + 1))
  {
    return term(t + 1) && rightExpression(t + 1);
  }
  else
  {
    ausgabe("$", t + 1);
    return 1;
  }
}

int expression(int t)
{
  ausgabe("expression->", t);
  return term(t + 1) && rightExpression(t + 1);
}
int num(int t)
{
  ausgabe("num->", t);
  if (lookAhead(digitSet))
  {
    return digit(t + 1) && num(t + 1);
  }
  else
  {
    return digit(t + 1);
  }
}
