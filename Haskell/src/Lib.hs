module Lib
    ( someFunc,
      numParse
    ) where

import Text.ParserCombinators.Parsec

-- default stuff
someFunc :: IO ()
someFunc = putStrLn "someFunc"

numParse :: IO ()
numParse = putStrLn "hello, numparse!"

-- NOTE:
-- The original example was implemented using Java
-- and worked on the following grammar rules:
-- num -> digit | digit num
-- digit -> '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '0'


--------------------------------------------------------------
-- Grammar Functions
--------------------------------------------------------------

-- expression -> term rightExpression
-- rightExpression -> '+' term rightExpression
-- rightExpression -> '-' term rightExpression
-- rightExpression -> \epsilon (empty word)
-- term -> operator rightTerm
-- rightTerm -> '*' operator rightTerm
-- rightTerm -> '/' operator rightTerm
-- rightTerm -> \epsilon (empty word)
-- operator -> '(' expression ')' | num
-- num -> digit | digit num
-- digit -> '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' | '0'


--------------------------------------------------------------
-- Helper Functions
--------------------------------------------------------------
